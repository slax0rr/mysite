---
title: "About"
date: 2020-03-28T08:59:32+02:00
draft: false
---

My name is Tomaž, also known online as slax0r. You can find me on the
IRC network Libera Chat  or reach out to me on Twitter at @lo_wreck.

With over 10 years of experience in software development, I have worked across
various domains, including system and web development. In recent years, I have
shifted my focus primarily to web development, where I’ve led the creation of
robust web applications, custom e-commerce systems, and RESTful APIs. My
technical expertise includes PHP, Typo3 CMS, and the administration of Linux/BSD
and Windows servers.

I consider myself a meticulous coder, strongly believing that code should not
only function well but also be beautifully crafted. I take pride in delivering
high-availability solutions, such as payment gateways and content management
systems, and I’m passionate about optimizing user experiences through clean,
efficient code.

## Skills

Throughout my years of experience in software development and computer science,
I have acquired a diverse set of skills in programming languages, technologies,
and systems. My expertise includes:

- **Programming Languages:** Go, Python, PHP, C/C++, ASP.NET
- **Web Technologies:** HTML, CSS, JavaScript
- **Database Management:** SQL, CockroachDB
- **Scripting:** Bash
- **Containerization & Orchestration:** Docker, Kubernetes
- **Networking & Messaging:** Cilium, NATS, NATS JetStream
- **Additional Technologies:** And many more...

I am continually expanding my knowledge and staying current with industry trends
to deliver effective and efficient solutions.

## Career

#### 2008 - Kapion d.o.o., Murska Sobota, Slovenia

* Software Developer
* System Administrator

Developed web applications using the PHP programming language and the Typo3
Content Management System. Installed and managed Asterix PBX systems, while
also administering Linux/BSD and Windows servers to ensure smooth operations.

#### 2009 - Proficio d.o.o., Maribor, Slovenia

* Software Developer
* System Administrator

Contributed to the ongoing development of insurance back-office software,
built a custom Content Management System, and developed various web and mobile
applications. Managed SQL, web, mail, and backup servers, alongside
administering the company's local network. Additionally, gained experience in
project planning and coordination for company-wide initiatives.

#### 2010-2013 - Proplatform d.o.o., Slovenska Bistrica (Slovenia)

* Software Developer
* System Administrator

Further development of the insurance back-office software, development of a
custom Content Management System, and other web applications, and development of
mobile applications. Administration of SQL, Web, Mail, and Backup servers, and
administration of the company's local network. Also learned about planning
different projects for the company.

#### 2013-2015 - Nekom Customer Care GmbH, Graz (Austria)

* Software Developer
* System Administrator

Led the development of web applications, custom e-commerce platforms, and
designed RESTful APIs and SOAP web services. Additionally responsible for the
administration of Linux/BSD servers and source control management, ensuring
efficient operations and seamless deployments.

#### 2015-2016 - TimeTac GmbH, Graz (Austria)

* Backend Developer

Developed and maintained the backend for a custom time tracking and attendance
monitoring software, ensuring accurate and reliable data management and
processing. Focused on delivering robust functionality and seamless integration
with front-end systems.

#### 2016-2018 - Advanced Commerce Labs GmbH, Graz (Austria)

* Senior Software Developer
* Technical Project Manager

Developed PHP-based backend solutions for various e-commerce platforms and
custom integration systems. Built and maintained web services, handled
administration of Linux/BSD servers, and oversaw source control management.
Managed one of the company's largest e-commerce systems, ensuring optimal
performance and reliability.

#### 2018-2020 - Viberate - decentralized live music marketplace, Ljubljana (Slovenia)

* Senior Backend Developer
* System Administrator
* DevOps

Responsible for the end-to-end development of the entire service-oriented backend
system powering [viberate.com](https://viberate.com). This includes setting up source
control, implementing continuous integration and deployment pipelines, as well
as managing the production Kubernetes cluster to ensure scalability and
reliability from the ground up.

#### 2020-2022 Form3 Financial Cloud, London (United Kingdom) - Remote

* **Senior Software Engineer**

Developing a high-availability, high-traffic payment gateway for a UK payment
scheme, leveraging a microservices architecture. Focused on delivering a robust,
scalable platform capable of handling millions of transactions daily while
ensuring optimal performance and reliability.
Tech used:
- Go
- Bash
- Docker
- Kubernetes
- CockroachDB
- NATS
- Flux
- Cilium
- Terraform

#### 2022-present Form3 Financial Cloud, London (United Kingdom) - Remote

* **Lead Engineer - Payment Architecture Development**

Spearheading a team of engineers in the design and development of a cutting-edge
multi-payment scheme gateway for a UK payment system. Responsible for ensuring
high availability and seamless performance in a high-traffic environment,
processing millions of transactions daily. The platform is built on a
microservices architecture, optimizing scalability and reliability to meet the
demands of the payment industry.

Tech used:
- Go
- Bash
- Docker
- Kubernetes
- AWS
- GCP
- CockroachDB
- NATS
- NATS JetStream
- Flux
- Terraform
